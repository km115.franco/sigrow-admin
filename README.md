# Sigrow Admin

> This is a web ui admin to check the values from sensors and send to a modbus slave emulator

- [Sigrow Admin](#sigrow-admin)
  - [Requirements](#requirements)
    - [Redis publisher](#redis-publisher)
    - [Redis Subscriber](#redis-subscriber)
    - [NodeJS](#nodejs)
  - [Setup](#setup)
  - [Raspberry setup as hotspot](#raspberry-setup-as-hotspot)
    - [Step 1 - Install requirements](#step-1---install-requirements)
    - [Step 2 - Hostapd Configuration](#step-2---hostapd-configuration)
    - [Step 3 - Dnsmasq Configuration](#step-3---dnsmasq-configuration)
    - [Step 4 - DHCP Configuration](#step-4---dhcp-configuration)
    - [Step 5 - Avoid channel blocking](#step-5---avoid-channel-blocking)
    - [Cleaning up - Disabling the hotspot](#cleaning-up---disabling-the-hotspot)
  - [Notes](#notes)

## Requirements

It is required to install `redis` to publish data to be available for other applications like this one

```bash
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis
```

### Redis publisher

A modification for the script bellow was added to the main script to publish information to redis

```python
import redis
from time import sleep, ctime
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)

red.publish("message", dumps({
    "payload": str(message),
    "header": header,
}))

```

It is necessary to include the redis module on the virtual environment:

```bash
/home/pi/sigrow-role-cnv04-mqtt/venv/bin/pip install pyredis
```

### Redis Subscriber

To get the information from other scripts, the code bellow can help as model

```python
import redis
red = redis.StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)


def listener():
    sub = red.pubsub()
    sub.subscribe('message')
    for message in sub.listen():
        if message is not None and isinstance(message, dict):
            result = message.get('data')
            print(result)

listener()
```

### NodeJS

Node is required to run the api, and manage the processes with pm2

```bash
sudo apt install curl
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install nodejs
```

Then install the process manager and static server

```bash
sudo npm install -g pm2 serve
```

## Setup

Enable pm2 daemon and manage all applications with pm2

```bash
pm2 startup
pm2 start /home/pi/sigrow-admin/api/api.js --name admin-api
pm2 serve --spa --name admin-web /home/pi/sigrow-admin/build 8000
pm2 start /home/pi/sigrow-admin/api/modbus/main.py --interpreter=/home/pi/sigrow-role-cnv04-mqtt/venv/bin/python --name modbus
pm2 save
```

## Raspberry setup as hotspot

The following instructions were inspired from this source https://www.raspberryconnect.com/projects/65-raspberrypi-hotspot-accesspoints/168-raspberry-pi-hotspot-access-point-dhcpcd-method.

Official documentation is available here:
https://www.raspberrypi.com/documentation/computers/configuration.html

### Step 1 - Install requirements

Install requirements

```bash
sudo apt update
sudo apt upgrade
sudo apt install hostapd
sudo apt install dnsmasq
```

Stop the services to make the configurations required

```bash
sudo systemctl stop hostapd
sudo systemctl stop dnsmasq
```

### Step 2 - Hostapd Configuration

Create the configuration file

```bash
sudo nano /etc/hostapd/hostapd.conf
```

then copy the following:

```bash
country_code=US
interface=wlan0
driver=nl80211
ssid=ArgusWireless
hw_mode=g
channel=6
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=12345678
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
```

Finally set the configuration files with the following code:

```bash
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
```

### Step 3 - Dnsmasq Configuration

Open the configuration file

```bash
sudo nano /etc/dnsmasq.conf
```

Go to the end of the file and add the following

```bash
#RPiHotspot config - No Intenet
interface=wlan0
domain-needed
bogus-priv
dhcp-range=192.168.50.150,192.168.50.200,255.255.255.0,12h
```

### Step 4 - DHCP Configuration

Open the configuration file

```bash
sudo nano /etc/dhcpcd.conf
```

At the end of the file add the folowing lines

```bash
#Static Hotspot
interface wlan0
nohook wpa_supplicant
static ip_address=192.168.50.10/24
static routers=192.168.50.1
```

### Step 5 - Avoid channel blocking

Add the following line to `/etc/rc.local` before the return sentence

```bash
sudo rfkill unblock wlan
```

### Cleaning up - Disabling the hotspot

```bash
sudo systemctl disable dnsmasq

sudo systemctl disable hostapd
```

Then remove the lines added on `/etc/dhcpcd.conf`

```bash
#Static Hotspot
interface wlan0
nohook wpa_supplicant
static ip_address=192.168.50.10/24
static routers=192.168.50.1
```

## Notes

To solve the unknown host issue when using sudo commands
https://linuxhandbook.com/sudo-unable-resolve-host/
