import axios from "axios";
import io from "socket.io-client";
export const host = `http://${window.location.hostname}:10000`;

export const socket = io.connect(`${host}`);

export const getDefaultCalibration = async () => {
  return await axios.get(`${host}/calibration/default`);
};

export const getCustomCalibration = async () => {
  return await axios.get(`${host}/calibration/custom`);
};

export const getDerivedCalc = async () => {
  return await axios.get(`${host}/calibration/derived`);
};

export const getMaxMins = async () => {
  return await axios.get(`${host}/calibration/maxmin`);
};

export const getModbusStatic = async () => {
  return await axios.get(`${host}/modbus`);
};

export const setDefaultCalibration = async (type) => {
  return await axios.post(`${host}/calibration/set/default`, { type });
};

export const setCustomCalibration = async (payload) => {
  return await axios.post(`${host}/calibration/custom`, payload);
};

export const setModbusAddr = async (addrs) => {
  return await axios.post(`${host}/modbus/set`, { addrs });
};

export const getDevices = async () => {
  return await axios.get(`${host}/devices`);
};
