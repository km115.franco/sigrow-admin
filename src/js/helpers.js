export const timestamp = () => {
  return new Date().toLocaleString("sv-SE");
};

export const format = (timestamp) => {
  return new Date(timestamp).toLocaleString("sv-SE");
};

export const addToDevices = (device, devices) => {
  const index = devices.findIndex((d) => d.remote_id === device.remote_id);

  if (index > -1) {
    devices[index] = device;
  } else {
    devices.push(device);
  }
  return devices;
};

export const orderByKey = (object, key) => {
  return object.sort((a, b) => a[key] - b[key]);
};

export const defaultCustomCalibration = {
  batt: "1*x",
  par: "1*x",
  temp: "1*x",
  humid: "1*x",
  soil_temp: "1*x",
  soil_hum: "1*x",
  pore_ec: "1*x",
  vp_air: "1*x",
  t_dew: "1*x",
  t_plant: "1*x",
  vp_plant: "1*x",
  vpd_plant: "1*x",
  weight_total: "1*x",
};

export const deviceType = {
  22: "Soil mini",
  12: "Soil pro",
  51: "Pixel",
  24: "Mass star",
};

export const registerTable = {
  22: [
    { label: "Battery percentage", key: "batt", unit: "%", calibration: true },
    { label: "PAR", key: "par", unit: "µmol/m²/s", calibration: true },
    { label: "Air temperature", key: "temp", unit: "°C", calibration: true },
    {
      label: "Air relative humidity",
      key: "humid",
      unit: "%",
      calibration: true,
    },
    { label: "Soil humidity", key: "soil_hum", unit: "%", calibration: true },
    {
      label: "Soil pore EC",
      key: "pore_ec",
      unit: "mS/cm",
      calibration: true,
    },
    {
      label: "Soil temperature",
      key: "soil_temp",
      unit: "°C",
      calibration: true,
    },
  ],
  12: [
    { label: "Battery percentage", key: "batt", unit: "%", calibration: true },
    { label: "PAR", key: "par", unit: "µmol/m²/s", calibration: true },
    { label: "Air temperature", key: "temp", unit: "°C", calibration: true },
    {
      label: "Air relative humidity",
      key: "humid",
      unit: "%",
      calibration: true,
    },
    { label: "Soil humidity", key: "soil_hum", unit: "%", calibration: true },
    {
      label: "Soil pore EC",
      key: "pore_ec",
      unit: "mS/cm",
      calibration: true,
    },
    {
      label: "Soil temperature",
      key: "soil_temp",
      unit: "°C",
      calibration: true,
    },
    {
      label: "VPD Air",
      key: "vp_air",
      unit: "kPa",
      calibration: true,
    },
    {
      label: "T Dew",
      key: "t_dew",
      unit: "°C",
      calibration: true,
    },
  ],
  51: [
    { label: "Battery percentage", key: "batt", unit: "%", calibration: true },
    { label: "PAR", key: "par", unit: "µmol/m²/s", calibration: true },
    { label: "Air temperature", key: "temp", unit: "°C", calibration: true },
    {
      label: "Air relative humidity",
      key: "humid",
      unit: "%",
      calibration: true,
    },
    {
      label: "VPD Air",
      key: "vp_air",
      unit: "kPa",
      calibration: true,
    },
    {
      label: "T Plant",
      key: "t_plant",
      unit: "°C",
      calibration: true,
    },
    {
      label: "T Dew",
      key: "t_dew",
      unit: "°C",
      calibration: true,
    },
    {
      label: "VPD Plant",
      key: "vpd_plant",
      unit: "kPa",
      calibration: true,
    },
  ],
  24: [
    { label: "Battery percentage", key: "batt", unit: "%", calibration: true },
    { label: "PAR", key: "par", unit: "µmol/m²/s", calibration: true },
    { label: "Air temperature", key: "temp", unit: "°C", calibration: true },
    {
      label: "Total weight",
      key: "weight_total",
      unit: "g",
      calibration: true,
    },
  ],
};
