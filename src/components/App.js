import { useEffect, useState } from "react";
import { addToDevices } from "../js/helpers";
import Debug from "./Debug";
import DeviceInfo from "./DeviceInfo";
import Devices from "./Devices";
import Modbus from "./Modbus";
import Navigation from "./Navigation";
import { socket } from "../js/api";
import Calibration from "./Calibration";

const App = () => {
  const [tab, setTab] = useState("devices");
  const [devices, setDevices] = useState([]);
  const [id, setId] = useState();
  const [update, setUpdate] = useState({});

  useEffect(() => {
    setDevices((d) => addToDevices(update, d));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [update]);

  useEffect(() => {
    socket.on("device", (msg) => {
      setUpdate(msg);
    });
    return () => {
      socket.off("device");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="is-flex centered">
      <Navigation tab={tab} setTab={setTab} />

      <div
        style={{
          marginTop: "5em",
          height: "calc(100vh - 5em)",
          overflow: "scroll",
          padding: "2em",
          maxWidth: "1024px",
        }}
      >
        {tab === "devices" && (
          <Devices
            setId={setId}
            devices={devices}
            setDevices={setDevices}
            setTab={setTab}
          />
        )}
        {tab === "debug" && <Debug />}
        {tab === "modbus" && <Modbus devices={devices} />}
        {tab === "calibration" && <Calibration />}
        {tab === "info" && <DeviceInfo devices={devices} id={id} />}
      </div>
    </div>
  );
};

export default App;
