import { format } from "timeago.js";
import { getDeviceType } from "../js/helpers";

const DeviceCard = ({ device }) => {
  const { time, remote_id, type, index } = device;
  return (
    <div
      className="card has-background-primary "
      style={{ width: "10em", cursor: "pointer" }}
    >
      <div className="card-content">
        <div className="media">
          <div className="media-content">
            <p className="title is-4 has-text-white">
              {getDeviceType(type)} {remote_id}
            </p>
          </div>
        </div>
        <div className="content has-text-white">
          <small>Address: {index}</small>
          <hr className="m-0" />
          <small>{format(time)}</small>
        </div>
      </div>
    </div>
  );
};
export default DeviceCard;
