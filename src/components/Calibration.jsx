import { useEffect, useState } from "react";
import { JsonEditor as Editor } from "jsoneditor-react";

import {
  getCustomCalibration,
  getDefaultCalibration,
  getDerivedCalc,
  getMaxMins,
} from "../js/api";

const Calibration = () => {
  const [calCustom, setCalCustom] = useState();
  const [calDefault, setCalDefault] = useState();
  const [derived, setDerived] = useState();
  const [maxMins, setMaxMins] = useState();
  const handleSave = () => {
    // saveSettings(settings);
    console.log("saving settings");
  };

  useEffect(() => {
    getCustomCalibration().then((res) => {
      setCalCustom(res.data);
    });
    getDefaultCalibration().then((res) => {
      setCalDefault(res.data);
    });
    getDerivedCalc().then((res) => {
      setDerived(res.data);
    });
    getMaxMins().then((res) => {
      setMaxMins(res.data);
    });
  }, []);

  return (
    <section>
      <h1 className="title is-4">Default Calibration</h1>
      {calDefault && (
        <Editor
          value={calDefault}
          onChange={(e) => setCalDefault(e)}
          mainMenuBar={false}
          navigationBar={false}
          enableSort={false}
          mode={Editor.modes.code}
        />
      )}
      <br />

      <h1 className="title is-4">Custom Calibration</h1>
      {calCustom && (
        <div style={{ width: "25em" }}>
          <Editor
            value={calCustom}
            onChange={(e) => setCalCustom(e)}
            mainMenuBar={false}
            navigationBar={false}
            enableSort={false}
            mode={Editor.modes.code}
          />
        </div>
      )}

      <button
        onClick={handleSave}
        className="mt-4 button is-primary is-outlined"
        style={{ margin: "1em" }}
      >
        Save
      </button>
    </section>
  );
};
export default Calibration;
