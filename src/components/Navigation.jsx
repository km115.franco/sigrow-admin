import { useState } from "react";
import ArgusLogo from "./ArgusLogo";

const Navigation = ({ setTab, tab }) => {
  const handleTab = (e) => {
    setTab(e.target.id);
  };

  return (
    <nav
      className="navbar is-flex between"
      style={{
        position: "fixed",
        top: 0,
        width: "100vw",
        borderRadius: 0,
      }}
    >
      <div className="navbar-brand">
        <a
          href="#nosotros"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ArgusLogo />
        </a>
      </div>
      <div className="is-flex centered">
        <a
          className="navbar-item"
          href="#devices"
          id="devices"
          onClick={handleTab}
          style={{ height: "100%", padding: "1.5em 1em" }}
        >
          Devices
        </a>
        <a
          className="navbar-item"
          href="#Debug"
          id="debug"
          onClick={handleTab}
          style={{ height: "100%", padding: "1.5em 1em", marginRight: "1em" }}
        >
          Debug
        </a>
      </div>
    </nav>
  );
};
export default Navigation;
