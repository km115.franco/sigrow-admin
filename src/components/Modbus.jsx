import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { getModbusStatic, setModbusAddr } from "../js/api";
import { orderByKey } from "../js/helpers";

const Modbus = ({ devices }) => {
  const [modbus, setModbus] = useState([]);
  const [loading, setLoading] = useState(false);
  const [orderBy, setOrderBy] = useState("remote_id");
  const [ordered, setOrdered] = useState([]);
  const [message, setMessage] = useState({ text: "", type: "" });

  const options = [
    { value: "remote_id", name: "Device Id" },
    { value: "index", name: "Modbus Address" },
  ];
  const handleErase = (id) => {
    setModbus((modbus) => modbus.filter((m) => m.id !== id));
  };

  const handleAdd = () => {
    if (!modbus.find((m) => m.id === "")) {
      setModbus((m) => [...m, { id: "", address: "" }]);
    }
  };

  const handleChangeKey = (e, id) => {
    const temp = [...modbus];
    const newId = e.target.value;
    const index = temp.findIndex((m) => m.id === id);
    temp[index].id = newId;
    setModbus(temp);
  };

  const handleChangeValue = (e, addr) => {
    const temp = [...modbus];
    const newAddr = e.target.value;
    const index = temp.findIndex((m) => m.address === addr);
    temp[index].address = newAddr;
    setModbus(temp);
  };

  const handleSave = () => {
    setLoading(true);
    const result = {};
    modbus.forEach((e) => {
      if (e.id) {
        result[e.id] = parseInt(e.address);
      }
    });
    setModbusAddr(result).then((res) => {
      setLoading(false);
      console.log(res);
    });
  };

  const hasDuplicates = (arr, target) => {
    return arr.filter((x) => x === target).length > 1;
  };

  const hasDuplicatesArr = (arr) => {
    const addrs = arr.map((x) => x.address);
    const ids = arr.map((x) => x.id);
    return (
      new Set(addrs).size !== addrs.length || new Set(ids).size !== ids.length
    );
  };

  const validateInput = () => {
    return (
      hasDuplicatesArr(modbus) ||
      modbus.some((m) => m.address > 255) ||
      modbus.some((m) => m.address < 1) ||
      modbus.some((m) => m.id > 65536) ||
      modbus.some((m) => m.id < 1)
    );
  };

  const validateRow = (arr, m) => {
    return (
      hasDuplicates(
        arr.map((x) => x.address),
        m.address
      ) ||
      hasDuplicates(
        arr.map((x) => x.id),
        m.id
      ) ||
      m.address > 255 ||
      m.address < 1 ||
      m.id > 65536 ||
      m.id < 1
    );
  };

  useEffect(() => {
    setOrdered(orderByKey(devices, orderBy));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [orderBy]);

  useEffect(() => {
    getModbusStatic().then((res) => {
      const structure = [];

      Object.keys(res.data)
        .sort((a, b) => a - b)
        .forEach((key) => {
          structure.push({
            id: parseInt(key),
            address: parseInt(res.data[key]),
          });
        });
      setModbus(structure);
    });
  }, []);

  return (
    <div>
      <h1 className="title">Modbus</h1>
      <h1 className="title is-4">Static Addresses</h1>
      <div className="is-flex vertical centered" style={{ gap: "0.5em" }}>
        {modbus.map((element, index) => (
          <div
            key={index}
            className={`is-flex centered ${
              validateRow(modbus, element) && "has-text-danger"
            }`}
          >
            <label>Device ID: </label>
            <input
              type="number"
              className="input  has-text-right"
              value={element.id}
              style={{ width: "5em", marginLeft: "0.2em", paddingRight: 0 }}
              onChange={(e) => handleChangeKey(e, element.id)}
            />
            <label style={{ marginLeft: "1em" }}>Modbus address: </label>
            <input
              type="number"
              className="input has-text-right"
              value={element.address}
              onChange={(e) => handleChangeValue(e, element.address)}
              style={{ width: "5em", marginLeft: "0.2em" }}
            />
            <button
              className="button is-danger ml-1"
              onClick={() => handleErase(element.id)}
            >
              <span className="icon">
                <FontAwesomeIcon icon={faCircleXmark} />
              </span>
            </button>
          </div>
        ))}
        <button
          className="button is-success"
          onClick={handleAdd}
          disabled={"" in modbus}
          style={{ width: "10em" }}
        >
          Add new address
        </button>
        <br />
        <button
          className={`button is-primary ${loading && "is-loading"}`}
          disabled={loading || validateInput()}
          onClick={handleSave}
        >
          *Save
        </button>
        <p className="help is-danger">
          *Saving changes will restart the modbus list
        </p>
      </div>
      <hr />
      <h1 className="title is-4">Device List</h1>
      <div className="is-flex vertical centered">
        <div className="is-flex centered">
          <label>Order by: </label>
          <span className="select ml-1">
            <select
              onChange={(e) => {
                setOrderBy(e.target.value);
              }}
            >
              {options.map((o) => (
                <option value={o.value}>{o.name}</option>
              ))}
            </select>
          </span>
        </div>
        <table className="table is-striped is-narrow">
          <thead>
            <tr>
              <th> Device ID </th>
              <th> Modbus Addresses </th>
            </tr>
          </thead>
          <tbody>
            {ordered.map((d, i) => (
              <tr key={i}>
                <td className="has-text-right"> {d.remote_id} </td>
                <td className="has-text-right"> {d.index} </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default Modbus;
