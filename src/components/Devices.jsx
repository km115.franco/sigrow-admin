import { useEffect, useState } from "react";
import { getCustomCalibration, getDevices } from "../js/api";
import DeviceInfo from "./DeviceInfo";

const Devices = ({ devices, setDevices }) => {
  const [calibration, setCalibration] = useState({});

  useEffect(() => {
    getDevices().then((res) => {
      setDevices(res.data.devices);
    });

    getCustomCalibration().then((res) => {
      setCalibration(res.data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <section>
      <h1 className="subtitle is-5">Device count: {devices.length}</h1>
      <div
        className="is-flex"
        style={{
          flexWrap: "wrap",
          gap: "1em",
        }}
      >
        {devices.map((device, i) => (
          <div key={i}>
            <DeviceInfo
              setDevices={setDevices}
              devices={devices}
              id={device.remote_id}
              calibration={calibration}
              setCalibration={setCalibration}
            />
          </div>
        ))}
      </div>
    </section>
  );
};
export default Devices;
