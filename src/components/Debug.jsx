import { faPause } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { socket } from "../js/api";
import { timestamp } from "../js/helpers";

const Debug = () => {
  const [debug, setDebug] = useState();
  const [debugMessages, setDebugMessages] = useState([]);
  const [pause, setPause] = useState(false);
  const handlePause = () => {
    setPause((p) => !p);
  };

  useEffect(() => {
    socket.on("debug", (msg) => {
      setDebug(msg);
    });

    return () => {
      socket.off("debug");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (debug && !pause) {
      const time = timestamp();
      setDebugMessages((m) => {
        return [{ time, debug }, ...m];
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debug]);

  return (
    <div>
      <section className="hero">
        <div className="hero-body">
          <p className="title">Debug</p>
          <p className="subtitle">See the incoming messages from devices</p>
          <button className="button" onClick={handlePause}>
            <FontAwesomeIcon icon={faPause} className="icon" />
            <span>{pause ? "Restart" : "Pause"}</span>
          </button>
        </div>
      </section>

      {debugMessages.map((m, i) => (
        <pre
          key={i}
          className={`help code ${"type" in m.debug && "has-text-primary"}`}
        >
          {JSON.stringify(m, null, 2)}
        </pre>
      ))}
    </div>
  );
};
export default Debug;
