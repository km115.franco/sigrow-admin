import {
  defaultCustomCalibration,
  deviceType,
  format,
  registerTable,
} from "../js/helpers";
import * as timeago from "timeago.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleXmark, faSave } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import { setCustomCalibration, setModbusAddr } from "../js/api";

const DeviceInfo = ({
  setDevices,
  devices,
  id,
  calibration,
  setCalibration,
}) => {
  const device = devices.find((d) => d.remote_id === id);

  const { type, time, index, remote_id } = device;
  console.log(device);
  console.log("type", type);

  const [add, setAdd] = useState(index);
  const [loading, setLoading] = useState(false);

  const [initialCal, setInitialCal] = useState({
    ...defaultCustomCalibration,
    ...calibration[remote_id],
  });
  const [dCal, setDCal] = useState({
    ...defaultCustomCalibration,
    ...calibration[remote_id],
  });
  const [customCalcs, setCustomCalcs] = useState({});
  const [validEq, setValidEq] = useState(true);

  const validateAdd = () => {
    return (
      add === index ||
      "" === add ||
      isNaN(add) ||
      parseInt(add) < 1 ||
      parseInt(add) > 255
    );
  };

  const validateRepeated = () => {
    const inUse = devices.filter((d) => d.index !== index).map((d) => d.index);
    return inUse.includes(parseInt(add));
  };

  const handleSave = () => {
    setLoading(true);
    const result = {};
    devices.forEach((d) => {
      result[d.remote_id] = parseInt(d.index);
    });
    console.log({ result });
    setModbusAddr({ ...result, [remote_id]: parseInt(add) }).then((res) => {
      setLoading(false);
      const newDevices = devices.map((ds) =>
        ds.remote_id === remote_id ? { ...ds, index: parseInt(add) } : ds
      );
      setDevices(newDevices);
    });
  };

  const processValue = (device, item) => {
    try {
      switch (item.key) {
        case "time":
          return format(device[item.key]);
        case "batt":
          return Math.round(device[item.key]);
        case "soil_temp":
          return device[item.key].toFixed(2);
        case "t_dew":
          return device[item.key].toFixed(2);
        case "t_plant":
          return device[item.key].toFixed(2);
        case "temp":
          return device[item.key].toFixed(2);
        case "pore_ec":
          return device[item.key].toFixed(2);
        case "soil_hum":
          return device[item.key].toFixed(2);
        case "vp_air":
          return device[item.key].toFixed(2);
        case "vpd_plant":
          return device[item.key].toFixed(2);
        case "par":
          return device[item.key].toFixed(0);
        default:
          return device[item.key];
      }
    } catch (error) {
      return device[item.key];
    }
  };

  const removeDevice = () => {
    setDevices(devices.filter((d) => d.remote_id !== remote_id));
  };

  const handleCalChange = (e) => {
    const { id, value } = e.target;
    setDCal((d) => ({ ...d, [id]: value }));
  };

  const objDiff = (o1, o2) => {
    return JSON.stringify(o1) != JSON.stringify(o2);
  };

  const handleSaveEq = () => {
    const payload = {};
    Object.keys(dCal).forEach((key) => {
      if (dCal[key] !== defaultCustomCalibration[key]) {
        payload[key] = dCal[key];
      }
    });
    setDevices((device) =>
      device.map((d) =>
        d.remote_id !== remote_id ? d : { ...d, ...customCalcs }
      )
    );
    setLoading(true);
    setCustomCalibration({ ...calibration, [remote_id]: payload }).then(() => {
      setCalibration((c) => ({ ...c, [remote_id]: payload }));
      setInitialCal(dCal);
      setLoading(false);
    });
  };

  const forceDecimals = (val) => {
    try {
      return val.toFixed(2);
    } catch (error) {
      return val;
    }
  };

  useEffect(() => {
    if (dCal) {
      let valid = true;
      const processUserCal = (raw, calib) => {
        let result = NaN;
        try {
          const lambda = (x) => {
            return eval(calib);
          };
          result = lambda(raw);
        } catch (error) {
          valid = false;
        }
        return result;
      };
      const results = {};
      Object.keys(dCal).forEach((key) => {
        if (objDiff(initialCal, dCal)) {
          results[key] = processUserCal(device.raw[key], dCal[key]);
        }
      });

      setCustomCalcs(results);
      setValidEq(valid);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dCal]);

  return (
    <>
      {device?.type && (
        <div style={{ marginBottom: "2em" }}>
          <h1 className="title is-4">
            {deviceType[type]}: {id}
          </h1>
          <h1 className="subtitle is-6">Last update: {timeago.format(time)}</h1>
          <div className="is-flex" style={{ alignItems: "center" }}>
            <p>Modbus address:</p>
            <input
              name="modbus"
              className={`input has-text-right ${add != index && "is-primary"}`}
              value={add}
              onChange={(e) => setAdd(e.target.value)}
              style={{ width: "5em", marginLeft: "1em" }}
            />
            <button
              className={`button is-success ml-1 is-small ${
                loading && "is-loading"
              }`}
              disabled={validateAdd() || validateRepeated()}
              onClick={() => handleSave(device)}
              title="Save address address"
            >
              <span className="icon">
                <FontAwesomeIcon icon={faSave} />
              </span>
            </button>
            {/* <button
          className="button is-primary ml-1 is-small"
          disabled={add == index}
          onClick={() => setAdd(index)}
          title="Show previous address"
        >
          <span className="icon">
            <FontAwesomeIcon icon={faRotateLeft} />
          </span>
        </button> */}
            <button
              className="button is-danger ml-1 is-small"
              onClick={removeDevice}
              title="Remove device from list"
            >
              <span className="icon">
                <FontAwesomeIcon icon={faCircleXmark} />
              </span>
            </button>
            {validateRepeated() && (
              <p className="help is-danger ml-1">Address in use</p>
            )}
          </div>
          <table class="table is-striped">
            <thead>
              <tr>
                <td>Modbus Register</td>
                <th>Variable</th>
                <td>
                  Calibration
                  {objDiff(initialCal, dCal) && (
                    <button
                      className="button is-small is-success ml-1"
                      disabled={!validEq}
                      onClick={handleSaveEq}
                    >
                      <span className="icon">
                        <FontAwesomeIcon icon={faSave} />
                      </span>
                    </button>
                  )}
                </td>
                <td>Latest value</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {registerTable[type].map((item, index) => (
                <tr key={index}>
                  <td> {index + 3} </td>
                  <th> {item.label} </th>
                  {item.calibration ? (
                    <td className="is-flex">
                      <input
                        className={`input has-text-right ${
                          objDiff(initialCal[item.key], dCal[item.key]) &&
                          "is-info"
                        }`}
                        value={dCal[item.key]}
                        id={item.key}
                        onChange={handleCalChange}
                        style={{ width: "8em" }}
                        placeholder={defaultCustomCalibration[item.key]}
                      />

                      {objDiff(initialCal[item.key], dCal[item.key]) && (
                        <p
                          className={` ${
                            objDiff(initialCal[item.key], dCal[item.key]) &&
                            "has-text-info"
                          }`}
                          style={{ padding: "0.5em" }}
                        >
                          {forceDecimals(customCalcs[item.key])}
                        </p>
                      )}
                    </td>
                  ) : (
                    <td></td>
                  )}

                  <td className="has-text-right">
                    {processValue(device, item) || 0}
                  </td>
                  <td>{item.unit}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};
export default DeviceInfo;
