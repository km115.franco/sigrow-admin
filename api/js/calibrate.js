const { readFile } = require("./helpers");

const getDefaultCalibration = () => {
  const path = __dirname.replace("api/js", "api/json/calibration_default.json");
  return readFile(path);
};

const getCustomCalibration = () => {
  const path = __dirname.replace("api/js", "api/json/calibration_custom.json");
  return readFile(path);
};

const getDerivedCalc = () => {
  const path = __dirname.replace("api/js", "api/json/derived_calc.json");
  return readFile(path);
};

const getMaxMins = () => {
  const path = __dirname.replace("api/js", "api/json/maxmin.json");
  return readFile(path);
};

const applyCalibration = (raw, calibration) => {
  Object.keys(calibration).forEach((measurement) => {
    const f = (x) => {
      try {
        // eslint-disable-next-line no-eval
        return eval(calibration[measurement]);
      } catch (error) {
        return NaN;
      }
    };
    raw[measurement] = f(raw[measurement]);
  });
  return raw;
};

const calcDerived = (calibrated, derivedCalc, custom) => {
  Object.keys(derivedCalc).forEach((measurement) => {
    const f = (x) => {
      try {
        // eslint-disable-next-line no-eval
        return eval(derivedCalc[measurement]);
      } catch (error) {
        console.log(error);
        return NaN;
      }
    };
    console.log(measurement);
    calibrated.raw[measurement] = f(calibrated);
    calibrated[measurement] = f(calibrated);
    if (measurement in custom) {
      const f = (x) => {
        try {
          // eslint-disable-next-line no-eval
          return eval(custom[measurement]);
        } catch (error) {
          console.log(error);
          return NaN;
        }
      };
      calibrated[measurement] = f(calibrated[measurement]);
    }
  });
  return calibrated;
};

const calcMaxMin = (calibrated, maxMins) => {
  const limited = {};
  Object.keys(maxMins).forEach((measurement) => {
    if (calibrated[measurement] > maxMins[measurement].max)
      limited[measurement] = maxMins[measurement].max;
    if (calibrated[measurement] < maxMins[measurement].min)
      limited[measurement] = maxMins[measurement].min;
  });
  return { ...calibrated, ...limited };
};

const calibrate = (data) => {
  const custom = getCustomCalibration();
  const defaults = getDefaultCalibration();
  let calibrated;
  const defaultCal = applyCalibration(data, defaults[data.type]);
  calibrated = {
    ...defaultCal,
    custom: false,
    raw: { ...defaultCal },
  };
  if (data.remote_id in custom) {
    const customCal = applyCalibration(data, custom[data.remote_id]);
    calibrated = {
      ...calibrated,
      ...customCal,
      custom: true,
    };
  }
  return calibrated;
};

const derived = (calibrated) => {
  const derivedCalc = getDerivedCalc();
  let custom = getCustomCalibration();
  if (calibrated.remote_id in custom) {
    custom = custom[calibrated.remote_id];
  }

  let newCalcs;
  if (calibrated.type in derivedCalc) {
    newCalcs = calcDerived(calibrated, derivedCalc[calibrated.type], custom);
  }
  return newCalcs;
};

const limited = (calibrated) => {
  const maxMins = getMaxMins();
  if (calibrated.type in maxMins) {
    calibrated = calcMaxMin(calibrated, maxMins[calibrated.type]);
  }
  return calibrated;
};

exports.calibrate = calibrate;
exports.derived = derived;
exports.limited = limited;
