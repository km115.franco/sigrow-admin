const toUInt = (string, start, len) => {
  return parseInt(string.slice(start, start + len), 16);
};

const toSignedInt = (string, start, len = 4) => {
  let unsignedInt = parseInt(string.slice(start, start + len), 16);
  // Check if the most significant bit is set
  if (unsignedInt & 0x8000) {
    // Convert to signed integer
    return -((~unsignedInt + 1) & 0xffff);
  } else {
    return unsignedInt;
  }
};

const decode22 = (payload) => {
  return {
    type: toUInt(payload, 0, 2),
    len: toUInt(payload, 2, 4),
    central_id: toUInt(payload, 6, 4),
    remote_id: toUInt(payload, 10, 4),
    batt: toUInt(payload, 14, 4),
    rssi: toUInt(payload, 18, 4),
    tsample: toUInt(payload, 22, 4),
    par: toUInt(payload, 26, 4),
    temp: toUInt(payload, 30, 4),
    humid: toUInt(payload, 34, 4),
    soil_hum: toUInt(payload, 38, 4),
    soil_temp: toUInt(payload, 42, 4),
    soil_ec: toUInt(payload, 46, 4),
    time: new Date(),
  };
};

const decode12 = (payload) => {
  return {
    type: toUInt(payload, 0, 2),
    len: toUInt(payload, 2, 4),
    central_id: toUInt(payload, 6, 4),
    remote_id: toUInt(payload, 10, 4),
    batt: toUInt(payload, 14, 4),
    rssi: toUInt(payload, 18, 4),
    tsample: toUInt(payload, 22, 4),
    par: toUInt(payload, 26, 4),
    temp: toUInt(payload, 30, 4),
    humid: toUInt(payload, 34, 4),
    soil_hum: toUInt(payload, 38, 4),
    soil_temp: toUInt(payload, 42, 4),
    soil_ec: toUInt(payload, 46, 4),
    time: new Date(),
  };
};

const decode51 = (payload) => {
  return {
    type: toUInt(payload, 0, 2),
    len: toUInt(payload, 2, 4),
    central_id: toUInt(payload, 6, 4),
    remote_id: toUInt(payload, 10, 4),
    batt: toUInt(payload, 14, 4),
    rssi: toUInt(payload, 18, 4),
    tsample: toUInt(payload, 22, 4),
    par: toUInt(payload, 26, 4),
    temp: toUInt(payload, 30, 4),
    humid: toUInt(payload, 34, 4),
    ir: toUInt(payload, 36, 4),
    pixel_ambient_temp: toUInt(payload, 42, 4),
    pixel_object1_temp: toUInt(payload, 46, 4),
    pixel_object2_temp: toUInt(payload, 50, 4),
    time: new Date(),
  };
};

const decode24 = (payload) => {
  return {
    type: toUInt(payload, 0, 2),
    len: toUInt(payload, 2, 4),
    central_id: toUInt(payload, 6, 4),
    remote_id: toUInt(payload, 10, 4),
    batt: toUInt(payload, 14, 4),
    rssi: toUInt(payload, 18, 4),
    tsample: toUInt(payload, 22, 4),
    par: toUInt(payload, 26, 4),
    temp: toUInt(payload, 30, 4),
    weight1: toSignedInt(payload, 34, 4),
    weight2: toSignedInt(payload, 38, 4),
    weight3: toSignedInt(payload, 42, 4),
    weight4: toSignedInt(payload, 46, 4),
    soil_temp: toUInt(payload, 50, 4),
    wlevel: toUInt(payload, 54, 4),

    time: new Date(),
  };
};

const decode = (message) => {
  let { payload, header } = JSON.parse(message);
  payload = payload.slice(2, -1);
  switch (header) {
    case 22:
      return decode22(payload);
    case 12:
      return decode12(payload);
    case 51:
      return decode51(payload);
    case 24:
      return decode24(payload);

    default:
      return false;
  }
};

exports.decode = decode;
