const fs = require("fs");

const readFile = (path) => {
  let data = "{}";
  try {
    data = fs.readFileSync(path, { encoding: "utf8", flag: "r" });
  } catch (error) {
    fs.writeFileSync(path, "{}", { encoding: "utf8", flag: "w" });
  }
  return JSON.parse(data);
};

const getEmptySpace = (reserved, devices) => {
  const inUse = devices.map((d) => d.index).concat(reserved);
  const ordered = inUse.sort((a, b) => a - b);
  const maxVal = ordered[ordered.length - 1];
  let result = maxVal + 1;
  for (let i = 0; i < maxVal; i++) {
    if (!ordered.includes(i)) {
      result = i;
      break;
    }
  }
  return result;
};

const addToDevices = (device, devices, emitter, publisher) => {
  const path = __dirname.replace("api/js", "api/json/modbus_static.json");
  let static_addrs = {};
  static_addrs = readFile(path);
  const reserved_addrs = [...Object.values(static_addrs), 0];
  const index = devices.findIndex((d) => d.remote_id === device.remote_id);
  let payload;
  if (index > -1) {
    devices[index] = { ...device, index: devices[index].index };
    payload = { ...device, index: devices[index].index };
  } else {
    const static_add = static_addrs[device.remote_id];
    if (static_add) {
      devices.push({ ...device, index: static_add });
      payload = { ...device, index: static_add };
    } else {
      const newIndex = getEmptySpace(reserved_addrs, devices);
      static_addrs[device.remote_id] = newIndex;
      fs.writeFileSync(
        __dirname.replace("/api/js", "") + "/api/json/modbus_static.json",
        JSON.stringify(static_addrs, null, 4)
      );
      emitter("modbus", { message: "update" });

      devices.push({ ...device, index: newIndex });
      payload = { ...device, index: newIndex };
    }
  }
  emitter("device", payload);
  publisher.publish("device", JSON.stringify(payload));
  return devices;
};

exports.addToDevices = addToDevices;
exports.readFile = readFile;
