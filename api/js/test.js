// this script is intented to test complex derived formulas
const formulas = {
  pore_ec: "4*x.soil_ec/(1*x.soil_hum + 4)",
  vpd_air: "x.humid*10**(2.7857+(9.5*x.temp)/265.5+x.temp)",
  t_dew: "((17.27 * x.temp) / (237.7 + x.temp)) - (100 - x.humid)/5",
  vp_plant: "x.humid*10**(2.7857+(9.5*x.temp_leaf)/265.5+x.temp_leaf)",
  vpd_plant: "x.vp_plant-x.vpd_air",
  weight_total: "x.weight1 + x.weight2 + x.weight3 + x.weight4",
};

const x = {
  soil_ec: 1,
  soil_hum: 2,
  temp: 3,
  humid: 4,
  vp_plant: 3,
  vpd_air: 3,
  weight1: 3,
  weight2: 3,
  weight3: 3,
  weight4: 3,
};

const calc = (x, form) => {
  const result = eval(form);
  console.log(form);
  console.log(result);
  return result;
};

Object.keys(formulas).forEach((key) => {
  console.log(key);
  calc(x, formulas[key]);
});
