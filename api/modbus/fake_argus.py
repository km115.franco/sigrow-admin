#!/usr/bin/env python
# This script reads modbus devices, a multiparameter power meter and the modbus device in this repo

import serial
import minimalmodbus
# pip install minimalmodbus
from time import sleep

modbus_port = "/dev/ttyUSB1"
modbus_addr = 3

instrument = minimalmodbus.Instrument(modbus_port, modbus_addr)
instrument.serial.baudrate = 9600
instrument.serial.bytesize = 8
instrument.serial.parity = serial.PARITY_NONE
instrument.serial.stopbits = 1
instrument.serial.timeout = 0.1
instrument.mode = minimalmodbus.MODE_RTU
# instrument.debug = True


def read_reg(ins, reg, scale):
    return ins.read_register(reg, scale)


while True:
    print("\n\n_______ Reading device %d ______" % modbus_addr)
    registers = [
        {"address": 1, "key": "type", "scale": 0},
        {"address": 2, "key": "remote_id", "scale": 0},
        {"address": 3, "key": "batt", "scale": 0},
        {"address": 4, "key": "par", "scale": 0},
        {"address": 5, "key": "temp", "scale": 1},
        {"address": 6, "key": "humid", "scale": 0},
        {"address": 7, "key": "soil_hum", "scale": 1},
        {"address": 8, "key": "pore_ec", "scale": 1},
        {"address": 9, "key": "soil_temp", "scale": 1},
        {"address": 10, "key": "index", "scale": 0}
    ]
    try:
        for register in registers:

            print("reg: % d" % register["address"],
                  "\t\tkey:", register["key"],
                  "\t\tvalue:", read_reg(instrument, register["address"], register["scale"]))
    except:
        sleep(3)
    sleep(3)
