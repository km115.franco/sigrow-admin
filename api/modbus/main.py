from redis import StrictRedis
from modbus_tk import modbus_rtu
import sys

import modbus_tk
import modbus_tk.defines as cst
import serial

from threading import Thread
from json import loads, dumps

serial_port = '/dev/ttyUSB0'

redis = StrictRedis(
    'localhost', 6379, charset="utf-8", decode_responses=True)

count = 0
echo = False
slaves = {}
# Create the server
server = modbus_rtu.RtuServer(serial.Serial(serial_port, baudrate=19200))


def listener():
    global count
    global server
    global echo

    while True:
        sub = redis.pubsub()
        sub.subscribe('device')
        for message in sub.listen():
            if echo:
                print(message)
            if message is not None and isinstance(message, dict):
                data = message.get('data')
                if data != 1:
                    data = loads(data)
                    if echo:
                        print(data)
                    index = data["index"]

                    if index in slaves:
                        slave = slaves[index]["slave"]
                        modbus_updater(slave, data)
                    else:
                        slaves[index] = {
                            "slave": modbus_creator(data),
                        }


def modbus_updater(slave, data):
    print("...updated:", data["index"])

    scales = {
        22: [
            {"address": 1, "key": "type", "scale": 1},
            {"address": 2, "key": "remote_id", "scale": 1},
            {"address": 3, "key": "batt", "scale": 1},
            {"address": 4, "key": "par", "scale": 1},
            {"address": 5, "key": "temp", "scale": 10},
            {"address": 6, "key": "humid", "scale": 1},
            {"address": 7, "key": "soil_hum", "scale": 10},
            {"address": 8, "key": "pore_ec", "scale": 10},
            {"address": 9, "key": "soil_temp", "scale": 10},
            {"address": 10, "key": "index", "scale": 1}
        ],
        12: [
            {"address": 1, "key": "type", "scale": 1},
            {"address": 2, "key": "remote_id", "scale": 1},
            {"address": 3, "key": "batt", "scale": 1},
            {"address": 4, "key": "par", "scale": 1},
            {"address": 5, "key": "temp", "scale": 10},
            {"address": 6, "key": "humid", "scale": 1},
            {"address": 7, "key": "soil_hum", "scale": 10},
            {"address": 8, "key": "pore_ec", "scale": 10},
            {"address": 9, "key": "soil_temp", "scale": 10},
            {"address": 10, "key": "vp_air", "scale": 1},
            {"address": 11, "key": "t_dew", "scale": 1},
            {"address": 12, "key": "index", "scale": 1},
        ],
        51: [
            {"address": 1, "key": "type", "scale": 1},
            {"address": 2, "key": "remote_id", "scale": 1},
            {"address": 3, "key": "batt", "scale": 1},
            {"address": 4, "key": "par", "scale": 1},
            {"address": 5, "key": "temp", "scale": 10},
            {"address": 6, "key": "humid", "scale": 1},
            {"address": 7, "key": "vp_air", "scale": 10},
            {"address": 8, "key": "t_plant", "scale": 10},
            {"address": 9, "key": "t_dew", "scale": 10},
            {"address": 10, "key": "vpd_plant", "scale": 1},
            {"address": 11, "key": "index", "scale": 1}
        ],
        24: [
            {"address": 1, "key": "type", "scale": 1},
            {"address": 2, "key": "remote_id", "scale": 1},
            {"address": 3, "key": "batt", "scale": 1},
            {"address": 4, "key": "par", "scale": 1},
            {"address": 5, "key": "temp", "scale": 10},
            {"address": 6, "key": "weight_total", "scale": 1},
            {"address": 7, "key": "index", "scale": 1}
        ]
    }

    message = []
    print(data["remote_id"], data["index"])
    for scale in scales[data["type"]]:
        try:
            scaled = round(data[scale["key"]]*scale["scale"])
        except:
            print("none value error")
            scaled = 0
        slave.set_values('0', scale["address"], scaled)
        print('\t', scale["key"].ljust(10), '\t', round(
            data[scale["key"]], 2), '\t\t', scaled)
        message.append({"address": scale["address"], "value": scaled})


def filter_by_key(object, key, target):
    for id in object.keys():
        if object[id][key] == target:
            return object[id]


def modbus_creator(data):
    slave = server.add_slave(data["index"])
    slave.add_block('0', cst.HOLDING_REGISTERS, 0, 100)
    modbus_updater(slave, data)
    return slave


logger = modbus_tk.utils.create_logger(
    name="console", record_format="%(message)s")

logger.info("... redis listener")

redis_listener = Thread(target=listener)
redis_listener.start()


# Info server:
try:
    logger.info("... running...")
    logger.info("... enter 'quit' for closing the server")
    logger.info("... enter 'e' for data echo")
    logger.info("... enter 'c' devices count")
    logger.info("... enter 's' show slaves")
    server.start()
    while True:
        print("\n")
        cmd = sys.stdin.readline()
        args = cmd.split(' ')

        if cmd.find('quit') == 0:
            print('bye-bye\r\n')
            redis_listener.join()
            break

        if cmd.find('e') == 0:
            print('echo:', not echo)
            echo = not echo

        if cmd.find('c') == 0:
            print(":", "device count:", len(slaves.keys()))

        if cmd.find('s') == 0:
            print(":", "slaves:", list(slaves.keys()))

finally:
    server.stop()
