const redis = require("redis");
const publisher = redis.createClient();
let demo = require("./demo_data");
let index = 0;

// demo = demo.filter((x) => x.header === 22);
publisher.connect().then(() => {
  setInterval(() => {
    publisher.publish("message", JSON.stringify(demo[index])).then(() => {
      console.log("...demo:", index);
      index += 1;
      if (index >= demo.length) index = 0;
    });
  }, 1000);
});
