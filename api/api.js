const express = require("express");
const cors = require("cors");
const http = require("http");
const socketio = require("socket.io");
const redis = require("redis");
const { decode } = require("./js/decode");
const { calibrate, derived, limited } = require("./js/calibrate");
const { addToDevices, readFile } = require("./js/helpers");
const { writeFileSync } = require("fs");
const { execSync } = require("child_process");

const client = redis.createClient();
const publisher = client.duplicate();
const subscriber = client.duplicate();

subscriber.connect();
publisher.connect();

const app = express();
app.use(cors());
app.use(express.json());

const server = http.createServer(app);
const io = socketio(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

let devices = [];

app.get("/", (req, res) => {
  res.send({ message: "gps server working, waiting for commands" });
});

app.get("/devices", (req, res) => {
  res.send({ devices });
});

app.get("/calibration/custom", (req, res) => {
  const calibration = readFile(__dirname + "/json/calibration_custom.json");
  res.send(calibration);
});

app.post("/calibration/custom", (req, res) => {
  const calibration = req.body;
  writeFileSync(
    __dirname + "/json/calibration_custom.json",
    JSON.stringify(calibration, null, 4)
  );
  res.send(calibration);
});

app.get("/calibration/default", (req, res) => {
  const calibration = readFile(__dirname + "/json/calibration_default.json");
  res.send(calibration);
});

app.get("/calibration/derived", (req, res) => {
  const result = readFile(__dirname + "/json/derived_calc.json");
  res.send(result);
});

app.get("/calibration/maxmin", (req, res) => {
  const result = readFile(__dirname + "/json/maxmin.json");
  res.send(result);
});

app.get("/modbus", (req, res) => {
  const result = readFile(__dirname + "/json/modbus_static.json");
  res.send(result);
});

app.post("/modbus/set", (req, res) => {
  writeFileSync(
    __dirname + "/json/modbus_static.json",
    JSON.stringify(req.body.addrs, null, 4)
  );
  try {
    execSync("pm2 restart modbus");
  } catch (error) {}
  devices = [];

  res.send({ message: "ok" });
});

io.on("connection", (socket) => {
  console.log("... a user connected");
  setTimeout(() => {
    socket.emit("driver", { status: "driver connected" });
  }, 3000);
  socket.on("disconnect", () => {
    console.log("... user disconnected");
  });
});

const emitter = (type, message) => {
  io.sockets.emit(type, message);
};

subscriber.subscribe("message", (message) => {
  const newMessage = JSON.parse(message);
  const { payload, header } = JSON.parse(message);

  console.log({ header, payload: payload.slice(0, 10) + " ..." }); // 'message'
  emitter("debug", newMessage); // 'message'
  const data = decode(message);
  if (data) {
    emitter("debug", data); // 'message'
    const calibrated = calibrate(data);
    const derived_calc = derived(calibrated);
    console.log(derived_calc);
    let results = limited({ ...calibrated, ...derived_calc });
    devices = addToDevices(results, devices, emitter, publisher);
  }
});

const port = 10000;
server.listen(port, () => console.log("... listening on", port));
